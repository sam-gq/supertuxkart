#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

export DEB_LDFLAGS_MAINT_APPEND=-lGLU

%:
	dh $@ --buildsystem=cmake

override_dh_auto_clean:
	# Remove embedded libraries used by embedded Irrlicht
	rm -rf lib/mcpp/
	rm -rf lib/libsquish/

	dh_auto_clean

override_dh_auto_configure:
	# libbluetooth-dev is only available on linux, hence build with
	# wiimote input device support only on linux
	# TODO: remove -DBUILD_RECORDER=0 once libopenglrecorder is packaged
	if [ $(DEB_HOST_ARCH_OS) = linux ]; then \
	dh_auto_configure -- -DCMAKE_BUILD_TYPE="STKRelease" \
	                     -DSTK_INSTALL_BINARY_DIR="games" \
	                     -DSTK_INSTALL_DATA_DIR="share/games/supertuxkart" \
	                     -DUSE_CRYPTO_OPENSSL=OFF \
	                     -DBUILD_RECORDER=0; \
	else \
	dh_auto_configure -- -DCMAKE_BUILD_TYPE="STKRelease" \
	                     -DSTK_INSTALL_BINARY_DIR="games" \
	                     -DSTK_INSTALL_DATA_DIR="share/games/supertuxkart" \
	                     -DUSE_WIIUSE=OFF \
	                     -DUSE_CRYPTO_OPENSSL=OFF \
	                     -DBUILD_RECORDER=0; \
	fi

override_dh_install:
	# Licenses are all documented in debian/copyright
	find $(CURDIR)/debian/tmp \( \
	 -iname "LICENSE" \
	 -o -iname "License.txt" \
	 -o -iname "licence2.txt" \
	 -o -iname "Music_license.rtf" \
	 -o -iname "licenses.txt" \
	 -o -name "SIL Open Font License.txt" \
	\) -delete

	# Remove unneeded (developer) scripts and other files in data directory
	find $(CURDIR)/debian/tmp/usr/share/games/supertuxkart/data \( \
	 -name "optimize_data.sh" \
	 -o -name "pull_from_transifex.sh" \
	 -o -name "supertuxkart.appdata.xml" \
	 -o -name "supertuxkart.desktop" \
	 -o -name "*.icns" \
	 -o -name "*.plist" \
	 \) -delete

	# Remove files installed by embedded angelscript
	rm -r $(CURDIR)/debian/tmp/usr/include/angelscript.h \
		$(CURDIR)/debian/tmp/usr/lib/cmake/Angelscript/ \
		$(CURDIR)/debian/tmp/usr/lib/libangelscript.a

	dh_install -Xgitignore -Xgitattributes
