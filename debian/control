Source: supertuxkart
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Vincent Cheng <vcheng@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 glslang-dev,
 libalut-dev,
 libbluetooth-dev [linux-any],
 libcurl4-gnutls-dev | libcurl4-dev,
 libfreetype6-dev,
 libgl1-mesa-dev,
 libgles2-mesa-dev,
 libglu1-mesa-dev,
 libharfbuzz-dev,
 libjpeg-dev,
 libmbedtls-dev,
 libmcpp-dev,
 libopenal-dev,
 libpng-dev,
 libsdl2-dev,
 libsimde-dev,
 libsqlite3-dev,
 libsquish-dev,
 libvorbis-dev,
 libxrandr-dev,
 libxxf86vm-dev,
 mesa-common-dev,
 pkg-config,
 spirv-headers,
 spirv-tools,
 zlib1g-dev
Standards-Version: 4.6.1
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/games-team/supertuxkart.git
Vcs-Browser: https://salsa.debian.org/games-team/supertuxkart
Homepage: https://supertuxkart.net

Package: supertuxkart
Architecture: any
Depends:
 supertuxkart-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: 3D arcade racer with a variety of characters, tracks, and modes to play
 Karts. Nitro. Action! SuperTuxKart is a 3D open-source arcade racer with a
 variety of characters, tracks, and modes to play. The aim is to create a
 game that is more fun than realistic, and provide an enjoyable experience
 for all ages.
 .
 Discover the mystery of an underwater world, or drive through the jungles
 of Val Verde and visit the famous Cocoa Temple. Race underground or in a
 spaceship, through a rural farmland or a strange alien planet.
 Or rest under the palm trees on the beach, watching the other karts
 overtake you. But don't eat the bananas! Watch for bowling balls,
 plungers, bubble gum, and cakes thrown by your opponents.
 .
 You can do a single race against other karts, compete in one of several
 Grand Prix, try to beat the high score in time trials on your own, play
 battle mode against the computer or your friends, and more! For a greater
 challenge, race online against players from all over the world and prove
 your racing prowess!

Package: supertuxkart-data
Architecture: all
Multi-Arch: foreign
Depends:
 fonts-cantarell,
 fonts-noto-color-emoji,
 fonts-noto-core,
 fonts-noto-extra,
 fonts-noto-ui-core,
 fonts-noto-ui-extra,
 ${misc:Depends}
Suggests:
 supertuxkart
Description: 3D arcade racer with a variety of characters, tracks, and modes to play (data)
 Karts. Nitro. Action! SuperTuxKart is a 3D open-source arcade racer with a
 variety of characters, tracks, and modes to play. The aim is to create a
 game that is more fun than realistic, and provide an enjoyable experience
 for all ages.
 .
 Discover the mystery of an underwater world, or drive through the jungles
 of Val Verde and visit the famous Cocoa Temple. Race underground or in a
 spaceship, through a rural farmland or a strange alien planet.
 Or rest under the palm trees on the beach, watching the other karts
 overtake you. But don't eat the bananas! Watch for bowling balls,
 plungers, bubble gum, and cakes thrown by your opponents.
 .
 You can do a single race against other karts, compete in one of several
 Grand Prix, try to beat the high score in time trials on your own, play
 battle mode against the computer or your friends, and more! For a greater
 challenge, race online against players from all over the world and prove
 your racing prowess!
 .
 This package contains data files for SuperTuxKart.
